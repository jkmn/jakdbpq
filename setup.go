package jakdbpq

import (
	"database/sql"
	"errors"
	"flag"
	"fmt"
	"log"
	"reflect"
)

var adminSettings settings
var registeredTypes []registeredType

type registeredType struct {
	thing interface{}
	table string
}

// This init handles database setup.
func init() {
	// Command-Line Flags
	flag.StringVar(&adminSettings.Username, "dbauser", "", "The DB Admin Username, for DB setup and maintenance.")
	flag.StringVar(&adminSettings.Password, "dbapass", "", "The DB Admin Password, for DB setup and maintenance.")
}

func DBCheck() {
	// It only makes sense to do the DB check if the admin credentials are provided.

	if !flag.Parsed() {
		log.Fatal("Package jackdb requires you to parse the flags, preferably in your 'main' package.")
	}

	if adminSettings.Password == "" || adminSettings.Username == "" {
		// The admin credentials are not set.
		return
	}

	// The following variables should never vary.
	adminSettings.Database = "postgres"
	adminSettings.Host = sttngs.Host

	// Does the User exist?
	if exists, err := roleExists(); err != nil {
		log.Fatal("DBCHECK: An error occurred while checking for the role: " + err.Error())
	} else if !exists {
		if err := roleCreate(); err != nil {
			log.Fatal("DBCHECK: An error occurred while creating the role: " + err.Error())
		}
	}

	// Does the DB exist?
	if exists, err := dbExists(); err != nil {
		log.Fatal("DBCHECK: An error occurred while checking for the DB: " + err.Error())
	} else if !exists {
		if err := dbCreate(); err != nil {
			log.Fatal("DBCHECK: An error occurred while creating the DB: " + err.Error())
		}
	}
}

func dbCreate() error {
	setstring := fmt.Sprintf("dbname=%s host=%s user=%s password=%s", adminSettings.Database, adminSettings.Host, adminSettings.Username, adminSettings.Password)

	db, err := sql.Open("postgres", setstring)
	if err != nil {
		panic("localdb.DBCreate: Unable to open database:\n" + err.Error())
	}

	stmt, err := db.Prepare("CREATE DATABASE " + sttngs.Database + " OWNER " + sttngs.Username)
	if err != nil {
		return errors.New("localdb.DBCreate: Unable to prepare CREATE DATABASE statement:\n" + err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec()
	if err != nil {
		return errors.New("localdb.DBCreate: Unable to execute CREATE DATABASE statement:\n" + err.Error())
	}

	return nil
}

func dbExists() (bool, error) {
	setstring := fmt.Sprintf("dbname=%s host=%s user=%s password=%s", adminSettings.Database, adminSettings.Host, adminSettings.Username, adminSettings.Password)

	db, err := sql.Open("postgres", setstring)
	if err != nil {
		panic("localdb.OpenDB: Unable to open database:\n" + err.Error())
	}

	stmt, err := db.Prepare("SELECT datname FROM pg_database WHERE datname=$1")
	if err != nil {
		return false, errors.New("localdb.DBExists: Unable to prepare SELECT statement\n" + err.Error())
	}
	defer stmt.Close()

	rows, err := stmt.Query(sttngs.Database)
	if err != nil {
		return false, errors.New("localdb.DBExists: Unable to execute SELECT statement:\n" + err.Error())
	}
	defer rows.Close()
	return rows.Next(), nil
}

// The work to check registered types is not done immediately because of the complexity of parsing flags using the built-in parser.
// This simple method allows packages to register types in the init() function, which usually happens BEFORE flags are parsed.
// We request that the implementing package call 'TableCheck', which then does the work of actually checking for tables.
func RegisterType(thing interface{}, table string) {
	descriptor := reflect.TypeOf(thing).PkgPath() + "." + reflect.TypeOf(thing).Name()

	if reflect.TypeOf(thing).Kind() != reflect.Struct {
		log.Fatal("A registered type must be a struct. " + descriptor + " is not a struct.")
	}
	if table == "" {
		log.Fatal("A proper table name was not defined for " + descriptor + ".")
	}
	registeredTypes = append(registeredTypes, registeredType{thing, table})
}

func roleCreate() error {
	setstring := fmt.Sprintf("dbname=%s host=%s user=%s password=%s", adminSettings.Database, adminSettings.Host, adminSettings.Username, adminSettings.Password)

	db, err := sql.Open("postgres", setstring)
	if err != nil {
		panic("localdb.RoleCreate: Unable to open database:\n" + err.Error())
	}

	stmt, err := db.Prepare("CREATE ROLE " + sttngs.Username + " LOGIN PASSWORD '" + sttngs.Password + "'")
	if err != nil {
		return errors.New("localdb.RoleCreate: Unable to prepare CREATE ROLE statement:\n" + err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec()
	if err != nil {
		return errors.New("localdb.RoleCreate: Unable to execute CREATE ROLE statement:\n" + err.Error())
	}

	return nil
}

func roleExists() (bool, error) {
	setstring := fmt.Sprintf("dbname=%s host=%s user=%s password=%s", adminSettings.Database, adminSettings.Host, adminSettings.Username, adminSettings.Password)

	db, err := sql.Open("postgres", setstring)
	if err != nil {
		panic("localdb.RoleExists: Unable to open database:\n" + err.Error())
	}

	stmt, err := db.Prepare("SELECT usename FROM pg_catalog.pg_user WHERE usename=$1")
	if err != nil {
		return false, errors.New("localdb.RoleExists: Unable to prepare SELECT statement\n" + err.Error())
	}
	defer stmt.Close()

	rows, err := stmt.Query(sttngs.Username)
	if err != nil {
		return false, errors.New("localdb.RoleExists: Unable to execute SELECT statement:\n" + err.Error())
	}
	defer rows.Close()
	return rows.Next(), nil
}

// TableCheck checks for tables matching the appropriate type. As of right now, it's only intended for single-level structs.
func TableCheck() {

	for _, rt := range registeredTypes {

		columns := getColumns(rt.thing)

		// First, does the table exist?
		table_name := getTableName(rt.thing)
		if exists, err := tableExists(table_name); err != nil {
			log.Fatal("Failure to check for table (" + table_name + "): " + err.Error())
		} else if !exists {
			if err := tableCreate(table_name, columns); err != nil {
				log.Fatal("Failure to create table (" + table_name + "): " + err.Error())
			}
		}

		existing_columns, err := getTableColumns(table_name)
		if err != nil {
			log.Fatal("Failure to get columns for table (" + table_name + "): " + err.Error())
		}

		test_columns := make(map[string]column) // map[column_name]column
		for _, old_col := range existing_columns {
			test_columns[old_col.Name] = old_col
		}

		for _, col := range columns {
			test_col := test_columns[col.Name]
			if col.Type != test_col.Type {
				log.Fatalf("Data does not match table!\tcolumn: %s\tOld Type: %s\tNew Type: %s", col.Name, test_col.Type, col.Type)
			}
		}
	}
}

func getTableColumns(table string) ([]column, error) {
	// select column_name, data_type from information_schema.columns where table_name = 'config';
	jdb := NewConnection()

	query := "SELECT column_name, data_type from information_schema.columns where table_name=$1"
	stmt, err := jdb.prepare(query)
	if err != nil {
		return nil, errors.New("localdb.Tablecolumns: Unable to prepare SELECT statement:\n" + err.Error())
	}
	defer stmt.Close()

	rows, err := stmt.Query(table)
	if err != nil {
		return nil, errors.New("localdb.Tablecolumns: Unable to execute SELECT statement:\n" + err.Error())
	}

	columns := make([]column, 0) // map[column_name]data_type

	for rows.Next() {
		var col column

		if err = rows.Scan(&col.Name, &col.Type); err != nil {
			return nil, errors.New("localdb.Tablecolumns: Unable to scan data into memory:\n" + err.Error())
		}

		columns = append(columns, col)
	}

	return columns, nil
}

func tableCreate(table_name string, columns []column) error {
	jdb := NewConnection()

	query := "CREATE TABLE " + table_name + "( "
	for _, col := range columns {
		query += fmt.Sprint(col.Name, " ", col.Type)
		if col.PrimaryKey {
			query += fmt.Sprint(" CONSTRAINT ", table_name, "_key", " PRIMARY KEY,")
		} else {
			query += " NOT NULL,"
		}
	}
	query = query[:len(query)-1]
	query += " )"

	log.Println(query)

	stmt, err := jdb.prepare(query)
	if err != nil {
		return errors.New("localdb.TableCreate: Unable to prepare CREATE TABLE statement:\n" + err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec()
	if err != nil {
		return errors.New("localdb.TableCreate: Unable to execute CREATE TABLE statement:\n" + err.Error())
	}

	return nil
}

func tableExists(table string) (bool, error) {
	// SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' AND table_name='labels';

	jdb := NewConnection()

	stmt, err := jdb.prepare("SELECT table_name FROM information_schema.tables WHERE table_name=$1")
	if err != nil {
		return false, errors.New("localdb.DBExists: Unable to prepare SELECT statement\n" + err.Error())
	}
	defer stmt.Close()

	rows, err := stmt.Query(table)
	if err != nil {
		return false, errors.New("localdb.DBExists: Unable to execute SELECT statement:\n" + err.Error())
	}
	defer rows.Close()
	return rows.Next(), nil
}
