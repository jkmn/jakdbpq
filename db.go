package jakdbpq

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"reflect"
	"runtime/debug"
	"strconv"
	"strings"
)

// Third-party packages:
import (
	"database/sql"
	_ "github.com/lib/pq"
)

type JakDBPQ struct {
	db *sql.DB
}

type settings struct {
	Username string
	Password string
	Database string
	Host     string
}

type column struct {
	Name        string
	Type        string
	Value       interface{}
	PrimaryKey  bool
	Unique      bool
	NotNull     bool
	StructField int
	// Table      string
}

type Query struct {
	thing   interface{}
	filters []filter
	limit   int
}

type filter struct {
	Column   string
	Operator string
	Value    interface{}
}

var sttngs settings

func init() {

	// PULL DB SETTINGS FROM AN EXTERNAL SOURCE:

	var (
		file   *os.File
		err    error
		err_   error
		buf    *bytes.Buffer
		output []byte
	)

	// Open the file. If it doesn't exist, create it.

	if file, err_ = os.Open("dbsettings.json"); err_ != nil {
		if os.IsNotExist(err_) {
			log.Println("The file, dbsettings.json, does not exist. A new one is being created for you.")

			if file, err = os.Create("dbsettings.json"); err != nil {
				log.Println("Failure creating the new dbsettings.json: " + err.Error())
				file.Close()
			}
			if output, err = json.Marshal(sttngs); err != nil {
				log.Println("Failure to marshall settings into JSON: " + err.Error())
			}
			buf = bytes.NewBuffer(output)
			if _, err = buf.WriteTo(file); err != nil {
				log.Println("Failure to write settings into new dbsettings.json: " + err.Error())
			}
		}
		log.Fatalln("Failure opening dbsettings.json: " + err_.Error())
	}

	defer file.Close()

	// Read the file, unmarshall it into the package settings variable.

	buf = new(bytes.Buffer)
	if _, err = buf.ReadFrom(file); err != nil {
		log.Fatalln("Failure reading dbsettings.json: " + err.Error())
	}

	if err = json.Unmarshal(buf.Bytes(), &sttngs); err != nil {
		log.Fatalln("Failure decoding dbsettings.json: " + err.Error())
	}
}

func NewConnection() *JakDBPQ {
	jdb := new(JakDBPQ)

	setstring := fmt.Sprintf("dbname=%s host=%s user=%s password=%s", sttngs.Database, sttngs.Host, sttngs.Username, sttngs.Password)

	db, err := sql.Open("postgres", setstring)
	if err != nil {
		log.Fatalln("jakdb.OpenDB: Unable to open database:\n" + err.Error())
	}

	jdb.db = db

	return jdb
}

func (jdb *JakDBPQ) Delete(thing interface{}) error {

	pkey := getPrimaryKey(thing)
	table := getTableName(thing)

	stmt, err := jdb.prepare("DELETE FROM " + table + " WHERE " + pkey.Name + "=$1")
	if err != nil {
		return errors.New("jakdb.Delete: Unable to prepare DELETE statement:\n" + err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec(pkey.Value)
	if err != nil {
		return errors.New("jakdb.Delete: Unable to execute DELETE statement:\n" + err.Error())
	}

	return nil
}

func Exists(thing interface{}) (bool, error) {

	jdb := NewConnection()

	pkey := getPrimaryKey(thing)
	table := getTableName(thing)

	stmt, err := jdb.prepare("SELECT " + pkey.Name + " FROM " + table + " WHERE " + pkey.Name + "=$1")
	if err != nil {
		return false, errors.New("jakdb.Exists: Unable to prepare SELECT statement\n" + err.Error())
	}
	defer stmt.Close()

	rows, err := stmt.Query(pkey.Value)
	if err != nil {
		return false, errors.New("jakdb.Exists: Unable to execute SELECT statement:\n" + err.Error())
	}
	defer rows.Close()
	return rows.Next(), nil
}

func Index(thing interface{}) ([]string, error) {

	jdb := NewConnection()

	pkey := getPrimaryKey(thing)
	table := getTableName(thing)

	stmt, err := jdb.prepare("SELECT " + pkey.Name + " FROM " + table)
	if err != nil {
		return nil, errors.New("jakdb.Index: Unable to prepare SELECT statement.")
	}
	defer stmt.Close()

	var keys []string
	rows, err := stmt.Query()
	for rows.Next() {
		var key string
		rows.Scan(&key)
		keys = append(keys, key)
	}
	return keys, nil
}

func Insert(thing interface{}) error {

	jdb := NewConnection()

	cols := getColumns(thing)
	vals := make([]interface{}, 0)
	table := getTableName(thing)

	// Third, generate the prepared statement with the column names.
	query := "INSERT INTO " + table + " ("
	for _, col := range cols {
		query += col.Name + ","
		vals = append(vals, col.Value)
	}
	query = query[0 : len(query)-1] // Remove the trailing comma.
	query += ") VALUES ("
	for i := 1; i <= len(cols); i++ {
		query += "$" + strconv.Itoa(i) + ","
	}
	query = query[0 : len(query)-1] // Remove the trailing comma.
	query += ") "

	stmt, err := jdb.db.Prepare(query)
	if err != nil {
		return errors.New("jakdb.Insert: Unable to prepare INSERT statement:\n" + err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec(vals...)
	if err != nil {
		return errors.New("jakdb.Insert: Unable to execute INSERT statement:\n" + err.Error())
	}

	return nil
}

func NewSelect(thing interface{}) *Query {

	q := new(Query)
	q.thing = thing

	return q
}

func (q *Query) Filter(column string, operator string, value interface{}) *Query {
	column = strings.ToLower(column)
	q.filters = append(q.filters, filter{column, operator, value})
	return q
}

func (q *Query) Limit(limit int) *Query {
	q.limit = limit
	return q
}

func (q *Query) Execute() ([]interface{}, error) {

	jdb := NewConnection()

	//	pkey := getPrimaryKey(q.Thing)         // Primary key.
	table := getTableName(q.thing) // Table name.
	cols := getColumns(q.thing)    // List of column names.
	args := make([]interface{}, 0) // Query arguments.
	//	vals := make([]interface{}, len(cols))    // Intermediate values.
	vals_pt := make([]interface{}, len(cols)) // Returned value pointers.
	results := make([]interface{}, 0)         // Cumulative results, one per row.

	// Generate the prepared statement with the column names.
	query := "SELECT "
	for _, col := range cols {
		query += col.Name + ","
	}
	query = query[0 : len(query)-1] // Remove the trailing comma.
	query += " FROM " + table

	// Append filters.
	if len(q.filters) > 0 {
		query += " WHERE"

		for idx, filter := range q.filters {
			query += " " + filter.Column + filter.Operator + "$" + strconv.Itoa(idx+1) + " AND "
			args = append(args, filter.Value)
		}
		query = query[0 : len(query)-4] // Remove the trailing AND.

	}

	if q.limit > 0 {
		query += " LIMIT " + fmt.Sprintf("%d", q.limit)
	}

	// Prepare the query.
	stmt, err := jdb.prepare(query)
	if err != nil {
		log.Printf("Query:\n%s\n", query)
		return results, errors.New("jakdb.Execute: Unable to prepare SELECT statement:\n" + err.Error())
	}
	defer stmt.Close()

	// Execute the query.
	rows, err := stmt.Query(args...)
	if err != nil {
		return results, errors.New("jakdb.Execute: Unable to execute SELECT statement:\n" + err.Error())
	}

	//	log.Printf("Executed query:\n%#v\n", query)
	//	log.Printf("Query arguements:\n%#v\n", args)

	// Iterate over the rows and extract the data.
	for rows.Next() {
		thing_type := reflect.TypeOf(q.thing)
		new_thing := reflect.New(thing_type).Interface()
		thing_value := reflect.ValueOf(new_thing)

		// work-around to satisfy rows.Scan().
		for i, _ := range cols {
			vals_pt[i] = thing_value.Elem().Field(cols[i].StructField).Addr().Interface()
		}

		if err := rows.Scan(vals_pt...); err != nil {
			return results, errors.New("jakdb.Execute: Unable to scan data into memory:\n" + err.Error())
		}

		results = append(results, new_thing)
	}

	return results, nil
}

func (jdb *JakDBPQ) Select(thing interface{}) error {

	pkey := getPrimaryKey(thing)
	cols := getColumns(thing)
	vals := make([]interface{}, 0)
	table := getTableName(thing)

	// Third, generate the prepared statement with the column names.
	query := "SELECT "
	for _, col := range cols {
		query += col.Name + ","
		vals = append(vals, col.Value)
	}
	query = query[0 : len(query)-1] // Remove the trailing comma.
	query += " FROM " + table + " WHERE " + pkey.Name + "=$1"

	stmt, err := jdb.prepare(query)
	if err != nil {
		return errors.New("jakdb.Select: Unable to prepare SELECT statement:\n" + err.Error())
	}
	defer stmt.Close()

	rows, err := stmt.Query(pkey.Value)
	if err != nil {
		return errors.New("jakdb.Select: Unable to execute SELECT statement:\n" + err.Error())
	}
	rows.Next()

	err = rows.Scan(vals...)
	if err != nil {
		return errors.New("jakdb.Select: Unable to scan data into memory:\n" + err.Error())
	}

	return nil
}

func Update(thing interface{}) error {

	jdb := NewConnection()

	pkey := getPrimaryKey(thing)
	cols := getColumns(thing)
	vals := make([]interface{}, 0)
	table := getTableName(thing)

	query := "UPDATE " + table + " SET "
	for k, col := range cols {
		query += col.Name + "=$" + strconv.Itoa(k+1) + ","
		vals = append(vals, col.Value)
	}
	query = query[0 : len(query)-1]

	query += " WHERE " + pkey.Name + "=$" + strconv.Itoa(len(cols)+1)

	vals = append(vals, pkey.Value)

	stmt, err := jdb.prepare(query)
	if err != nil {
		return errors.New("jakdb.Update: Unable to prepare UPDATE statement:\n" + err.Error())
	}
	defer stmt.Close()

	// Fourth, execute the statement with the slice of values.
	_, err = stmt.Exec(vals...)
	if err != nil {
		return errors.New("jakdb.Update: Unable to execute UPDATE statement:\n" + err.Error())
	}

	return nil
}

func (jdb *JakDBPQ) prepare(query string) (*sql.Stmt, error) {
	return jdb.db.Prepare(query)
}

func Put(thing interface{}) error {

	if exists, err := Exists(thing); err != nil {
		return err
	} else if !exists {
		return Insert(thing)
	}
	return Update(thing)
}

func getPrimaryKey(thing interface{}) column {

	var col column

	cols := getColumns(thing)
	for _, test := range cols {
		if test.PrimaryKey {
			col = test
		}
	}

	return col
}

func getColumns(thing interface{}) []column {
	columns := make([]column, 0)

	thing_type := reflect.TypeOf(thing)
	//	table_name := tableName(thing)

	for i := 0; i < thing_type.NumField(); i++ {
		field := thing_type.Field(i)
		tag := field.Tag.Get("jackdb")

		var col column

		// Column Name:
		col.Name = strings.ToLower(field.Name)

		// Table Name:
		//		col.Table = table_name

		// Primary Key Status:
		col.PrimaryKey = (tag == "primary_key")

		// Struct Field identifier:
		col.StructField = i

		// Column Type:
		if tag != "-" {
			switch field.Type.Kind() {
			case reflect.Int64:
				col.Type = "bigint"
			case reflect.String:
				col.Type = "text"
			case reflect.Bool:
				col.Type = "boolean"
			case reflect.Int:
				col.Type = "integer"
			case reflect.Slice:
				col.Type = "text" // For JSON storage.
			case reflect.Struct:
				col.Type = "text" // For JSON storage.
			default:
				col.Type = field.Type.Name()
			}
		}

		// Column Value:
		col.Value = reflect.ValueOf(thing).Field(i).Interface()

		columns = append(columns, col)
	}
	//	log.Printf("Columns Map: %#v", columns)

	return columns
}

func getTableName(thing interface{}) string {
	descriptor := reflect.TypeOf(thing).PkgPath() + "." + reflect.TypeOf(thing).Name()

	var table string
	for _, rt := range registeredTypes {
		if reflect.TypeOf(thing) == reflect.TypeOf(rt.thing) {
			table = rt.table
		}
	}
	if table == "" {
		debug.PrintStack()
		log.Fatal("A table name was not found for " + descriptor + ".")
	}
	return table
}

//func tableName(thing interface{}) string {
//	thing_type := reflect.TypeOf(thing)
//	table_name := thing_type.PkgPath() + "_" + thing_type.Name()
//	table_name = strings.ToLower(table_name)
//	table_name = strings.Replace(table_name, "/", "_", -1)
//
//	return table_name
//}
