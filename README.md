jakdbpq
=======

Something like the App Engine Datastore interface for PostgreSQL.

There are lots of other cool packages that probably do a lot better job at providing an interface for databases, a big part of this was just to see if I could do it.

My own person requirements were as follows:
1. Use the sql package for maximum compatibility.
2. The interface should be able to given an object and be able to just deal with it.

With regards to #2, there are two limitations that come to mind.
First, because I wanted to use this with pre-existing projects, I needed to specificy a table name for various kinds of objects; I didn't want to rename tables. Thus, I ended up creating a registration method to register objects with tables.
Second, I'm using struct field tags, but I'm not sure that's strictly a failure to comply with #2.
